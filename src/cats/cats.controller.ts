import { Controller, Get, Req, Post, HttpCode, Header, Redirect, Param } from '@nestjs/common';
import { Request } from 'express'

@Controller('cats')
export class CatsController {

  /**
   * Get Index
   * How to access: localhost:3000/cats
   */
  @Get()
  getString(): string {
    return 'Return String';
  }

  /**
   * With path
   * How to access: localhost:3000/cats/profile
   */
  @Get('profile')
  getProfile(): string {
    return 'this is profile'
  }

  /**
   * Path with parameter request
   * @param request 
   */
  @Get('requestExample')
  getWithRequest(@Req() request: Request): string {
    if(request.res.statusCode === 200) {
      return 'Return if status code 200';
    } else {
      return 'error'
    }
  }

  /** 
   * Post example
   * How to access: localhost:3000/cats
   */
  @Post()
  create(): string {
    return 'This action adds a new cat';
  }

  /**
   * Post with http code
   * Won't output anything because HTTP Code 
   */
  @Post('statusCode')
  @HttpCode(204)
  createCat():string {
    return 'This action adds a new cat';
  }

  /**
   * Post with header
   */
  @Post('withHeader')
  @Header('Cache-Control', 'none')
  @Header('Allow-Access-Control', '*')
  header() {
    return 'Post with header'
  }

  /**
   * Redirection
   * if hit this path, will output text. not redirect to url. because status code = 200
   */
  @Get('redirection')
  @Redirect('https://chataja.co.id', 200)
  redirect() {
    return 'ok'
  }

  /**
   * Redirection
   * if hit this path, will redirect to url. because status code = 302
   */
  @Get('redirectionTo')
  @Redirect('https://chataja.co.id', 302)
  redirectTo() {
    return {url: 'https://chataja.co.id/'}
  }

  /**
   * Route with parameter id
   */
  @Get(':id')
  findById(@Param() params):string{
    console.log(params.id)
    return `id yang di hit adalah #${params.id}`
  }

  /**
   * Route with path parameter id
   */
  @Get('category/:id')
  findCategoryId(@Param() params):string{
    return `id kategori yang di hit adalah #${params.id}`
  }
  /**
   * 
   * @param id 
   */
  @Get('cat/:idx')
  findCatsById(@Param('idx') id:string):string{
    return `id kategori yang di hit adalah #${id}`
  }
}
